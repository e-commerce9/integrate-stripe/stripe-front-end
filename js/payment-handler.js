const showPayment = () => {
   
   $('#payment').modal({backdrop: false});
   $('#payment').modal('show');
}

document.getElementById('payment-form').addEventListener('submit', (evt) => {
   evt.preventDefault();
   //check if card is valid.
   const cardEmpty = document.getElementById('payment-form').getElementsByClassName('StripeElement--empty');
   const error = document.getElementById('payment-form').getElementsByClassName('StripeElement--invalid');
   console.log(error);
   if (error.length > 0 || cardEmpty.length > 0)
      return;

   $('#payment').modal('hide');
   showLoading();
   const productId = document.getElementById('pid').value;
   stripe.createToken(card).then((result) => {
      const price = document.getElementById('price').value;
      if (result.error) {
         const errorElement = document.getElementById('card-errors');
         errorElement.textContent = result.error.message;
         return;
      }
      // send product to server
      const requestBody = {
         product: {
            amount: price,
            description: ''
         }
      }
      const httpRequest = new XMLHttpRequest();
      httpRequest.onerror = function() {
         hideLoading();
         Swal.fire({
            'text': 'Purchasing product failed.',
            'icon': 'error'
         });
      }
      httpRequest.onload = function () {
         //server response back
         const responseJson = JSON.parse(this.response);
         const clientSecret = responseJson.clientSecret;
         stripe.confirmCardPayment(clientSecret, {
            payment_method: {
               card: card
            }
         }).then((result) => {
            hideLoading();
            if (result.error) {
               Swal.fire({
                  'text': 'Purchasing product failed.',
                  'icon': 'error'
               });
               return;
            }
            if (result.paymentIntent.status === 'succeeded') {
               Swal.fire({
                  'text': 'Thank you for purchasing our products.',
                  'icon': 'success'
               })
            }
         });
      }
      httpRequest.open('POST', 'http://localhost:3005/api/payment');
      httpRequest.setRequestHeader('content-type', 'application/json');
      httpRequest.send(JSON.stringify(requestBody));
   });
});

const showLoading = () => {
   const loading = document.getElementById('loading');
   const body = document.getElementsByTagName('body')[0];
   body.style.opacity = '0.7';
   body.style.pointerEvents = 'none';
   loading.classList.remove('d-none');
   loading.classList.add('d-block');
};

const hideLoading = () => {
   const loading = document.getElementById('loading');
   const body = document.getElementsByTagName('body')[0];
   body.style.opacity = '1';
   body.style.pointerEvents = 'auto';
   loading.classList.remove('d-block');
   loading.classList.add('d-none');
};
